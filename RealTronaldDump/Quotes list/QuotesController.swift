//
//  QuotesController.swift
//  RealTronaldDump
//
//  Created by Pablo on 12/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation
import TronaldDumpKit


class QuotesController {
    
    // MARK: - Properties
    
    let tag : Tag
    private(set) var quotesViewModels = [QuoteViewModel]()
    
    var onViewModelUpdate : (() -> Void)?
    
    
    // MARK: - Life cycle
    
    init(with tag : Tag) {
        self.tag = tag
        self.updateViewModelForCurrentTag()
    }
    
    
    private func updateViewModelForCurrentTag() {
        TronaldDump.quotes(for: tag) { [weak self] (result) in
            switch result {
            case .success(let quotes):
                self?.quotesViewModels = quotes.map({ QuoteViewModel(with: $0) })
                self?.onViewModelUpdate?()
                
            case .error(let requestError):
                print("⚠️ QuotesController: updateViewModelForCurrentTag: \(requestError)")
            }
        }
    }
}
