//
//  QuotesTableViewController.swift
//  RealTronaldDump
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import UIKit
import TronaldDumpKit


private let QuotesTableViewCellReuseID = "QuotesTableViewCellReuseID"


class QuotesTableViewController: UITableViewController {
    
    // MARK: - Properties
    
    var quotesController : QuotesController!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Quotes", comment: "")
        
        tableView.estimatedRowHeight = 95
        tableView.rowHeight = UITableViewAutomaticDimension
        
        quotesController.onViewModelUpdate = { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return quotesController.quotesViewModels.count
    }
    
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let quoteCell = tableView.dequeueReusableCell(withIdentifier: QuotesTableViewCellReuseID,
                                                      for: indexPath) as! QuoteTableViewCell
        
        let quoteMV = quotesController.quotesViewModels[indexPath.row]
        quoteCell.configure(with: quoteMV)
        
        return quoteCell
    }
}
