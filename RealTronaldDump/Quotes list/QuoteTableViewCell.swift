//
//  QuoteTableViewCell.swift
//  RealTronaldDump
//
//  Created by Pablo on 12/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import UIKit


class QuoteTableViewCell: UITableViewCell {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var authorAndDateLabel: UILabel!
}


extension QuoteTableViewCell {
    func configure(with quoteViewModel: QuoteViewModel) {
        textView.text = quoteViewModel.quoteText
        authorAndDateLabel.text = quoteViewModel.authorAndDate
    }
}

