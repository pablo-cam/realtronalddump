//
//  QuoteViewModel.swift
//  RealTronaldDump
//
//  Created by Pablo on 12/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//


import Foundation
import TronaldDumpKit


struct QuoteViewModel {
    let quoteText : String
    let authorAndDate : String
    
    static let dateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }()
}


extension QuoteViewModel {
    init(with quote: Quote) {
        self.quoteText = quote.text
        
        let authorAndDateFormat = NSLocalizedString("AuthorAndDateFormat", comment: "")
        let date = quote.creationDate
        let formattedDate = QuoteViewModel.dateFormatter.string(from: date)
        self.authorAndDate = String.localizedStringWithFormat(authorAndDateFormat, quote.author.name, formattedDate)
    }
}
