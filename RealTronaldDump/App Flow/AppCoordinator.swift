//
//  AppCoordinator.swift
//  RealTronaldDump
//
//  Created by Pablo on 12/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import UIKit
import TronaldDumpKit


let TagsTableVCStoryboardID   = "TagsTableVCStoryboardID"
let QuotesTableVCStoryboardID = "QuotesTableVCStoryboardID"


class AppCoordinator {
    
    // MARK: - Properties
    
    var navigationController : UINavigationController
    var storyBoard : UIStoryboard
    
    
    // MARK: - Initializers
    
    init(with navigationController: UINavigationController,
         storyBoard: UIStoryboard) {
        self.navigationController = navigationController
        self.storyBoard = storyBoard
        
        showTagsList()
    }
    
    
    private func showTagsList () {
        guard let tagsTableVC = storyBoard.instantiateViewController(withIdentifier: TagsTableVCStoryboardID) as? TagsTableViewController else {
            fatalError("🛑 AppCoordinator: Invalid storyboard ID for TagsTableViewController")
        }
        
        tagsTableVC.tagsController = TagsController()
        tagsTableVC.delegate = self
        navigationController.pushViewController(tagsTableVC, animated: false)
    }
    
    
    private func showQuotesList (for tag: Tag) {
        guard let quotesTableVC = storyBoard.instantiateViewController(withIdentifier: QuotesTableVCStoryboardID) as? QuotesTableViewController else {
            fatalError("🛑 AppCoordinator: Invalid storyboard ID for QuotesTableViewController")
        }
        
        quotesTableVC.quotesController = QuotesController(with: tag)
        navigationController.pushViewController(quotesTableVC, animated: true)
    }
}


extension AppCoordinator: TagsTableViewControllerDelegate {
    func tagsTableViewControllerDidSelectTag(_ tag: Tag) {
        showQuotesList(for: tag)
    }
}
