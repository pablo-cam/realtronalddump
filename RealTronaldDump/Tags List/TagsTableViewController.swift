//
//  TagsTableViewController.swift
//  RealTronaldDump
//
//  Created by Pablo on 11/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import UIKit
import TronaldDumpKit


private let TagsTableViewCellReuseID = "TagsTableViewCellReuseID"


protocol TagsTableViewControllerDelegate: class {
    func tagsTableViewControllerDidSelectTag(_ tag: Tag)
}


class TagsTableViewController: UITableViewController {
    
    // MARK: - Properties
    
    var tagsController : TagsController!
    weak var delegate  : TagsTableViewControllerDelegate?
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Tags", comment: "")
        
        tagsController.onViewModelUpdate = { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return self.tagsController.tags.count
    }
    
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TagsTableViewCellReuseID,
                                                 for: indexPath)
        
        cell.textLabel?.text = self.tagsController.tags[indexPath.row]
        
        return cell
    }
    
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedTag = self.tagsController.tags[indexPath.row]
        delegate?.tagsTableViewControllerDidSelectTag(selectedTag)
    }
}
