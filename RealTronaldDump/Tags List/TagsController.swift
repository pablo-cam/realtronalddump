//
//  TagsController.swift
//  RealTronaldDump
//
//  Created by Pablo on 12/12/2017.
//  Copyright © 2017 Pablo Camiletti. All rights reserved.
//

import Foundation
import TronaldDumpKit


class TagsController {
    
    // MARK: - Properties
    
    private(set) var tags = [Tag]()
    var onViewModelUpdate : (() -> Void)?
    
    
    // MARK: - Life cycle
    
    init() {
        loadTags()
    }
    
    
    func loadTags() {
        TronaldDump.allTags { [weak self] (result) in
            switch result {
            case .success(let allTags):
                self?.tags = allTags
                self?.onViewModelUpdate?()
                
            case .error(let requestError):
                print("⚠️ \(requestError)")
            }
        }
    }
}
